using System;
using Xunit;

namespace RST.Command.UnitTests
{
    public class DefaultMoveStrategyTestClass
    {
        
        [Theory]
        [InlineData(MoveDirection.Up,   10, 11)]
        [InlineData(MoveDirection.Down, 10, 9)]
        [InlineData(MoveDirection.Left, 9,  10)]
        [InlineData(MoveDirection.Right, 11, 10)]
        public void MoveUnit(MoveDirection moveDirection, uint expectedX, uint expectedY)
        {
            var moveStrategy = new DefaultMoveStrategy();
            var command = new MoveCommand()
            {
                MoveDirection = moveDirection
            };
            
            IMovable testUnit = new TestUnit();
            
            moveStrategy.DoAction(testUnit, command);
            
            Assert.Equal(expectedX, testUnit.PositionX);
            Assert.Equal(expectedY, testUnit.PositionY);
        }
    }
}
