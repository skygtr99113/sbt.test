using Xunit;

namespace RST.Command.UnitTests
{
    public class DefaultShootStrategy
    {
        [Fact]
        public void FireCommand_AmmunitionDecreasByRateOfFire()
        {
            var shooterStrategy = new DefaultShooterStrategy();
            
            IShooter testUnit = new TestUnit();
            uint expectedAmmunition = 9;
            var fireCommand = new FireCommand();
            
            shooterStrategy.DoAction(testUnit, fireCommand);
            Assert.Equal(expectedAmmunition, testUnit.Ammunition);
        }
        
        [Fact]
        public void ManyFireCommand_AmmunitionIsEmpty()
        {
            var shooterStrategy = new DefaultShooterStrategy();
            
            IShooter testUnit = new TestUnit();
            uint expectedAmmunition = 0;
            var fireCommand = new FireCommand();
            
            for(int i=0; i<20; i++)
                shooterStrategy.DoAction(testUnit, fireCommand);
            Assert.Equal(expectedAmmunition, testUnit.Ammunition);
        }
    }
}