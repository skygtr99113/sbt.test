﻿using System;
using System.Diagnostics;
using RST.Command;

namespace RST.Units.Helicopter
{
    public class Helicopter:IMovable, IRotatable, IShooter
    {
        public string Name { get; set; } = nameof(Helicopter);
        public uint PositionX { get; set; } = 0;
        public uint PositionY { get; set; } = 0;
        
        public void ExecuteCommand(ICommand command)
        {
            command.Execute(this);
        }

        uint IMovable.Speed { get; set; } = 20;

        IMoveStrategy  IMovable.MoveStrategy { get; set; }

        int IRotatable.Angle { get; set; } = 0;
        int IRotatable.RotateSpeed { get; set; } = 45;
        IRotateStrategy IRotatable.RotateStrategy { get; set; }
        public uint Ammunition { get; set; } = 1000;
        public uint RateOfFire { get; set; } = 30;
        public IShooterStrategy ShooterStrategy { get; set; }
    }
    
    public class HelicopterMoveStrategy: DefaultMoveStrategy
    {
        public override void DoAction(IMovable unit, MoveCommand command)
        {
            uint beforeX = unit.PositionX, beforeY = unit.PositionY;

            IRotatable rotatableUnit = unit as IRotatable;

            unit.PositionX = Convert.ToUInt32(Math.Cos(rotatableUnit.Angle * 0.01745328627d))*unit.Speed + beforeX;
            unit.PositionY = Convert.ToUInt32(Math.Sin(rotatableUnit.Angle * 0.01745328627d))*unit.Speed + beforeY;
            
            Trace.WriteLine($"{unit.Name.PadRight(10)} move from ({beforeX},{beforeY}) to ({unit.PositionX},{unit.PositionY})");
        }
    }

    public class HelicopterRotateStrategy: DefaultRotateStrategy
    {
        
    }
}
