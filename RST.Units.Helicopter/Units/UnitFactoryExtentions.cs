using System;
using RST.Command;

namespace RST.Units.Helicopter
{
    public static class UnitFactoryExtentions
    {
        private static readonly IMoveStrategy DefaultHelicopterMoveStrategy = new HelicopterMoveStrategy();
        private static readonly IRotateStrategy DefaultHelicopterRotateStrategy = new DefaultRotateStrategy();
        private static readonly IShooterStrategy DefaultHelicopterShootStrategy = new DefaultShooterStrategy();
        
        /// <summary>
        /// Создать юнит типа "Вертолёт"
        /// </summary>
        /// <param name="tankType">Тип вертолёта</param>
        /// <param name="startPositionX">Начальная позиция по оси X</param>
        /// <param name="startPositionY">Начальная позиция по оси Y</param>
        /// <param name="moveStrategy">Стратегия перемещения</param>
        /// <param name="shooterStrategy">Стратегия стрельбы</param>
        /// <param name="rotateStrategy">Стратегия разворота</param>
        /// <returns></returns>
        public static IUnit CreateHelicopter(this UnitFactory factory, Type helicopterType, uint startPositionX=0, uint startPositionY=0, 
            IMoveStrategy moveStrategy = null, IShooterStrategy shooterStrategy = null, 
            IRotateStrategy rotateStrategy = null)
        {
            var item = (IMovable)Activator.CreateInstance(helicopterType);
            item.MoveStrategy = moveStrategy??DefaultHelicopterMoveStrategy;
            ((IShooter) item).ShooterStrategy = shooterStrategy ?? DefaultHelicopterShootStrategy;
            ((IRotatable) item).RotateStrategy = rotateStrategy ?? DefaultHelicopterRotateStrategy;
            item.PositionX = startPositionX;
            item.PositionY = startPositionY;
            return item;
        }
    }
}