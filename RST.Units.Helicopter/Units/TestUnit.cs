using RST.Units.Helicopter;

[assembly:System.Runtime.CompilerServices.InternalsVisibleTo("RST.Units.Helicopter.UnitTests")]

namespace RST.Command
{
    internal class TestUnit:IMovable, IRotatable, IShooter
    {
        public string Name { get; set; } = nameof(Helicopter);
        public uint PositionX { get; set; } = 0;
        public uint PositionY { get; set; } = 0;
        
        public void ExecuteCommand(ICommand command)
        {
            command.Execute(this);
        }

        uint IMovable.Speed { get; set; } = 20;

        IMoveStrategy  IMovable.MoveStrategy { get; set; } 
        
        int IRotatable.Angle { get; set; }
        int IRotatable.RotateSpeed { get; set; } = 45;
        IRotateStrategy IRotatable.RotateStrategy { get; set; }
        public uint Ammunition { get; set; } = 1000;
        public uint RateOfFire { get; set; } = 30;
        public IShooterStrategy ShooterStrategy { get; set; }
    }
}