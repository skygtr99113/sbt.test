using RST.Units.Helicopter;

namespace RST.Command
{
    public enum RotateDirection
    {
        Right,
        Left
    }
    public partial class RotateCommand : ICommand
    {
        public RotateDirection RotateDirection = RotateDirection.Right;
        public void Execute(IUnit unit)
        {
            if (unit is IRotatable rotatableUnit)
            {
                rotatableUnit.RotateStrategy.DoAction(rotatableUnit, this);
            }
        }
    }
}