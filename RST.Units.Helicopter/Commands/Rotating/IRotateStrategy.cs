using System;
using System.Diagnostics;
using RST.Command;

namespace RST.Units.Helicopter
{
    public interface IRotateStrategy
    {
        void DoAction(IRotatable unit, RotateCommand command);
    }

    public class DefaultRotateStrategy : IRotateStrategy
    {
        public virtual void DoAction(IRotatable unit, RotateCommand command)
        {
            int beforeAngle = unit.Angle;
            unit.Angle = (command.RotateDirection == RotateDirection.Right)
                ? unit.Angle + unit.RotateSpeed
                : unit.Angle - unit.RotateSpeed;
            
            Trace.WriteLine($"{unit.Name.PadRight(10)} rotate from {beforeAngle} to {unit.Angle}");
        }
    }
}