using RST.Command;

namespace RST.Units.Helicopter
{
    public interface IRotatable:IUnit
    {
        int Angle { get; set; }
        int RotateSpeed { get; set; }
        IRotateStrategy RotateStrategy { get; set; }
    }
}