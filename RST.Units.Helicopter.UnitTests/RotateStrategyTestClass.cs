using System;
using RST.Command;
using Xunit;

namespace RST.Units.Helicopter.UnitTests
{
    public class RotateStrategyTestClass
    {
        [Theory]
        [InlineData(RotateDirection.Left, 1, -45)]
        [InlineData(RotateDirection.Left, 5, -225)]
        [InlineData(RotateDirection.Right, 1, 45)]
        [InlineData(RotateDirection.Right, 5, 225)]
        public void Rotate(RotateDirection rotateDirection, int count, int expectedAngle)
        {
            IRotatable testUnit = new TestUnit();
            IRotateStrategy rotateStrategy = new HelicopterRotateStrategy();
            var command = new RotateCommand()
            {
                RotateDirection = rotateDirection
            };
            
            for(int i=0; i<count; i++)
                rotateStrategy.DoAction(testUnit, command);
            
            Assert.Equal(expectedAngle, testUnit.Angle);
        }
    }
}
