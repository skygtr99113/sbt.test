﻿using System;
using System.Collections.Generic;
using RST.Command;
using RST.Units.Helicopter;

namespace SBT.Test
{
    class Program
    {
        
        static List<IUnit> AllUnits = new List<IUnit>(4);

        /// <summary>
        /// Генерирует юниты
        /// </summary>
        static void GenerateUnits()
        {
            // используя фабрику создаём нужные юниты, 
            // учитывая факт, что другие разработчики могли унаследовать типы юнитов из RST.Command
            // то логичнее передавать в фабрику тип юнита
            var factory = new UnitFactory();
            AllUnits.Add(factory.CreateCar(typeof(Car)));
            AllUnits.Add(factory.CreateTank(typeof(Tank)));
            AllUnits.Add(factory.CreateFence(typeof(Fence)));
            AllUnits.Add(factory.CreateHelicopter(typeof(Helicopter)));
        }

        /// <summary>
        /// Получает выделеные юниты
        /// </summary>
        /// <returns></returns>
        static IEnumerable<IUnit> GetSelectUnits()
        {
            // Предположим, что были выделены все 
            return AllUnits;
        }
        
        static void Main(string[] args)
        {
            GenerateUnits();

            // Команды для юнитов
            var commands = new List<ICommand>()
            { 
                new FireCommand(),
                new MoveCommand(),
                new RotateCommand(),
                new MoveCommand(),
                new MoveCommand(),
            };

            // Юниты
            var units = GetSelectUnits();

            // //Вариант 1: для каждой команды вызвать её исполнение на каждом юните.
            foreach (var command in commands)
            {
                foreach (var unit in units)
                {
                    command.Execute(unit);
                }
            }

            //Вариант 2: для каждого юнита вызвать исполнение команды на нём.
            /*
            foreach (var unit in units)
            {
                foreach (var command in commands)
                {
                    unit.ExecuteCommand(command);
                }
            }
            */

            Console.ReadKey();
        }
    }
}
