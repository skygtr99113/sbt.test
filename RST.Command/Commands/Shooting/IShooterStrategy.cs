using System;
using System.Diagnostics;

namespace RST.Command
{
    public interface IShooterStrategy
    {
        void DoAction(IShooter unit, FireCommand command);
    }

    public class DefaultShooterStrategy : IShooterStrategy
    {
        public virtual void DoAction(IShooter unit, FireCommand command)
        {
            if (unit.Ammunition > 0)
            {
                unit.Ammunition -= unit.RateOfFire;
                Trace.WriteLine($"{unit.Name.PadRight(10)} fire {unit.RateOfFire}. Ammunition: {unit.Ammunition}");
            }
            else
                Trace.WriteLine($"{unit.Name.PadRight(10)} oops, ammunition: 0");
            
        }
    }
}