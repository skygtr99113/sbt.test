namespace RST.Command
{
    public interface IShooter:IUnit
    {
        uint Ammunition { get; set; }
        uint RateOfFire { get; set; }
        IShooterStrategy ShooterStrategy { get; set; }
    }
    
}