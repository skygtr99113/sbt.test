namespace RST.Command
{
    public partial class FireCommand : ICommand
    {
        public void Execute(IUnit unit)
        {
            if (unit is IShooter unitShooter)
            {
                unitShooter.ShooterStrategy.DoAction(unitShooter, this);
            }
        }
    }
}