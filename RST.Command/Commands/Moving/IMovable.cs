namespace RST.Command
{
    public interface IMovable:IUnit
    {
        uint Speed { get; set; }
        IMoveStrategy MoveStrategy { get; set; }
    }
}