using System;
using System.Diagnostics;

namespace RST.Command
{
    /// <summary>
    /// Интерфейс стратегии перемещения юнита
    /// </summary>
    public interface IMoveStrategy
    {
        void DoAction(IMovable unit, MoveCommand command);
    }

    /// <summary>
    /// Стратегия перемещения юнита по умолчанию (начало координат в левом нижнем углу экрана, x-по горизонтали, y-по вертикали)
    /// </summary>
    public class DefaultMoveStrategy : IMoveStrategy
    {
        /// <summary>
        /// Реализует перемещение
        /// </summary>
        /// <param name="unit">Юнит, к которому будет применена стратегия перемещения</param>
        /// <param name="command">Команда перемещения</param>
        public virtual void DoAction(IMovable unit, MoveCommand command)
        {
            uint beforeX = unit.PositionX, beforeY = unit.PositionY;

            switch(command.MoveDirection)
            {
                case MoveDirection.Up:    unit.PositionY+=unit.Speed; break;
                case MoveDirection.Down:  unit.PositionY-=unit.Speed; break;
                case MoveDirection.Left:  unit.PositionX-=unit.Speed; break;
                case MoveDirection.Right: unit.PositionX+=unit.Speed; break;
            }
            

            Trace.WriteLine($"{unit.Name.PadRight(10)} move from ({beforeX},{beforeY}) to ({unit.PositionX},{unit.PositionY})");
        }
    }

    /// <summary>
    /// Класс реализующий стратегию перемещения автомобиля
    /// </summary>
    public class CarMoveStrategy:DefaultMoveStrategy
    {
    }

    /// <summary>
    /// Класс реализующий стратегию перемещения танка
    /// </summary>
    public class TankMoveStrategy:DefaultMoveStrategy
    {
    }

}