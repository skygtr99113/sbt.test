namespace RST.Command
{
    public enum MoveDirection
    {
        Up, Down, Left, Right
    }
    public partial class MoveCommand : ICommand
    {
        public MoveDirection MoveDirection = MoveDirection.Right;
        
        public void Execute(IUnit unit)
        {
            if(unit is IMovable movableUnit)
            {
                movableUnit.MoveStrategy.DoAction(movableUnit, this);
            }
        }
    }
}