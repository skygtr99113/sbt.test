﻿namespace RST.Command
{
    public class Tank : IMovable, IShooter
    {
        public string Name { get; set; } = nameof(Tank);
        public uint PositionX { get; set; }
        public uint PositionY { get; set; }
        uint IMovable.Speed { get; set; } = 2;

        IMoveStrategy  IMovable.MoveStrategy { get; set; }


        uint IShooter.Ammunition { get; set; } = 10;
        uint IShooter.RateOfFire { get; set; } = 1;
        IShooterStrategy IShooter.ShooterStrategy { get; set; }
        
        void IUnit.ExecuteCommand(ICommand command)
        {
            command.Execute(this);
        }

        
    }
}