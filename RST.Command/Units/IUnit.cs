﻿namespace RST.Command
{
    public interface IUnit 
    { 
        string Name { get; set; }
        uint PositionX {get;set;}
        uint PositionY {get;set;}
        void ExecuteCommand(ICommand command);
    }
}