﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace RST.Command
{
    public class Car: IMovable
    {
        public string Name { get; set; } = nameof(Car);
        public uint PositionX { get; set; }
        public uint PositionY { get; set; }


        uint IMovable.Speed { get; set; } = 10;

        IMoveStrategy  IMovable.MoveStrategy { get; set; }
        
        void IUnit.ExecuteCommand(ICommand command)
        {
            command.Execute(this);
        }
    }
}
