[assembly:System.Runtime.CompilerServices.InternalsVisibleTo("RST.Command.UnitTests")]

namespace RST.Command
{
    internal class TestUnit:IUnit, IMovable, IShooter
    {
        public string Name { get; set; } = nameof(Car);
        public uint PositionX { get; set; } = 10;
        public uint PositionY { get; set; } = 10;


        uint IMovable.Speed { get; set; } = 1;

        IMoveStrategy  IMovable.MoveStrategy { get; set; } 
        
        uint IShooter.Ammunition { get; set; } = 10;
        uint IShooter.RateOfFire { get; set; } = 1;
        IShooterStrategy IShooter.ShooterStrategy { get; set; } 
        
        void IUnit.ExecuteCommand(ICommand command)
        {
            command.Execute(this);
        }
    }
}