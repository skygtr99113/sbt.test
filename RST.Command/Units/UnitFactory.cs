using System;

namespace RST.Command
{
    public class UnitFactory
    {
        private static readonly IMoveStrategy DefaultCarMoveStrategy = new CarMoveStrategy();
        private static readonly IMoveStrategy DefaultTankMoveStrategy = new TankMoveStrategy();
        private static readonly IShooterStrategy DefaultShooterStrategy = new DefaultShooterStrategy();
        
        /// <summary>
        /// Создать юнит типа "Автомобиль"
        /// </summary>
        /// <param name="carType">Тип автомобиля</param>
        /// <param name="startPositionX">Начальная позиция по оси X</param>
        /// <param name="startPositionY">Начальная позиция по оси Y</param>
        /// <param name="moveStrategy">Стратегия перемещения</param>
        /// <returns></returns>
        public IUnit CreateCar(Type carType, uint startPositionX=0, uint startPositionY=0, IMoveStrategy moveStrategy=null)
        {
            var item = (IMovable)Activator.CreateInstance(carType);
            item.MoveStrategy = moveStrategy?? DefaultCarMoveStrategy;
            item.PositionX = startPositionX;
            item.PositionY = startPositionY;
            return item;
        }
        
        /// <summary>
        /// Создать юнит типа "Танк"
        /// </summary>
        /// <param name="tankType">Тип танка</param>
        /// <param name="startPositionX">Начальная позиция по оси X</param>
        /// <param name="startPositionY">Начальная позиция по оси Y</param>
        /// <param name="moveStrategy">Стратегия перемещения</param>
        /// <param name="shooterStrategy">Стратегия стрельбы</param>
        /// <returns></returns>
        public IUnit CreateTank(Type tankType, uint startPositionX=0, uint startPositionY=0, IMoveStrategy moveStrategy=null, IShooterStrategy shooterStrategy = null)
        {
            var item = (IMovable)Activator.CreateInstance(tankType);
            item.MoveStrategy = moveStrategy ?? DefaultTankMoveStrategy;
            ((IShooter) item).ShooterStrategy = shooterStrategy ?? DefaultShooterStrategy;
            item.PositionX = startPositionX;
            item.PositionY = startPositionY;
            return item;
        }
        
        /// <summary>
        /// Создать юнит типа "Стена"
        /// </summary>
        /// <param name="fenceType">Тип стены</param>
        /// <param name="startPositionX">Начальная позиция по оси X</param>
        /// <param name="startPositionY">Начальная позиция по оси Y</param>
        /// <returns></returns>
        public IUnit CreateFence(Type fenceType, uint startPositionX=0, uint startPositionY=0)
        {
            var item = (IUnit)Activator.CreateInstance(fenceType);
            item.PositionX = startPositionX;
            item.PositionY = startPositionY;
            return item;
        }
    }
}