﻿namespace RST.Command
{
    public class Fence : IUnit
    {
        public string Name { get; set; } = nameof(Fence);
        public uint PositionX { get; set; }
        public uint PositionY { get; set; }

        public void ExecuteCommand(ICommand command)
        {
            
        }
    }
}